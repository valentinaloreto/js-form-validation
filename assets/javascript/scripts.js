window.onload = function() {

  // *************************************************************VARIABLES
  let form = this.document.querySelector("#form");

  let name = this.document.querySelector('#name-id');
  let alertname = this.document.querySelector('#alert-name');
  let phone = this.document.querySelector('#phone-id');
  let alertphone = this.document.querySelector('#alert-phone');
  let email = this.document.querySelector('#email-id');
  let alertemail = this.document.querySelector('#alert-email');
  let comments = this.document.querySelector('#comments-id');
  let alertcomments = this.document.querySelector('#alert-comments');
  let select = this.document.querySelector('#select-options-id');
  let alertselect = this.document.querySelector('#alert-select');
  let alertradio = this.document.querySelector('#alert-radio');

  let checkboxes = this.document.querySelectorAll("input[type='checkbox']");
  let alertcheckbox = this.document.querySelector('#alert-checkbox');


  let testme = this.document.querySelector('#testme');

  //***************************************************************** CALLBACKS
  function validateName(){
    if (name.value.length < 4) {
      alertname.classList.add("showme");
      name.classList.add("border-red");
    } else {
      alertname.classList.remove("showme");
      name.classList.remove("border-red");
      name.classList.add("border-green");
    }
  }

  function validatePhone(){
    var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
    if(  !phone.value.match(phoneno) ) {
      alertphone.classList.add("showme");
      phone.classList.add("border-red");
    } else {
      alertphone.classList.remove("showme");
      phone.classList.remove('border-red');
      phone.classList.add('border-green');
    }
  }

  function validateEmail(){
    var emailno = /\S+@\S+\.\S+/;
    if(  !email.value.match(emailno) ) {
      alertemail.classList.add("showme");
      email.classList.add("border-red");
    } else {
      alertemail.classList.remove("showme");
      email.classList.remove('border-red');
      email.classList.add('border-green');
    }
  }

  function validateComments(){
    if (comments.value.length < 4) {
      alertcomments.classList.add("showme");
      comments.classList.add("border-red");
    } else {
      alertcomments.classList.remove("showme");
      comments.classList.remove("border-red");
      comments.classList.add("border-green");
    }
  }

  function validateSelect() {
    if (select.value == "") {
      alertselect.classList.add("showme");
      select.classList.add("border-red");
    } else {
      alertselect.classList.remove("showme");
      select.classList.remove("border-red");
      select.classList.add("border-green");
    }
  }

  function validateRadio() {
    let radiochecked = false
    form['gender'].forEach(function(el) {
      if (el.checked) {
        radiochecked = true
      }
    });
    if (!radiochecked) {
      alertradio.classList.add("showme");
    } else {
      alertradio.classList.remove("showme");
    }

  }

  function validateCheckbox() {
    let checkboxchecked = false
    checkboxes.forEach(function(el) {
      if (el.checked) {
        checkboxchecked = true
      }
    });
    if (!checkboxchecked) {
      alertcheckbox.classList.add("showme");
    } else {
      alertcheckbox.classList.remove("showme");
    }
  }

  // **************************************************************EVENT LISTENERS
  name.addEventListener('change', validateName);

  phone.addEventListener('change', validatePhone);

  email.addEventListener('change', validateEmail);

  comments.addEventListener('change', validateComments);

  testme.addEventListener('click', function (e){
    e.preventDefault();

    validateName();
    validatePhone();
    validateEmail();

    // selected
    validateSelect();
    // radio : array
    validateRadio();
    // checkbox
    validateCheckbox();

    validateComments();

  });

}

// in select, the value is the content of the input selected. You access it via the name of the input, and if no option is chosen, the value is ""
// in radio, you access if via the name of the input, which is repeated in all the options; this gives you an array, in which the chosen option has a value of true








// window.onload = function() {
//   if (window.jQuery) {
//       // jQuery is loaded
//       alert("Yeah!");
//   } else {
//       // jQuery is not loaded
//       alert("Doesn't Work");
//   }
// }



// let h1 = this.document.querySelector('h1')
  // h1.addEventListener('click', function (){
  //   alert(h1.innerHTML);
  // })
